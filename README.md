# 新肺确诊病例活动轨迹地图

#### 介绍
根据官方数据做的一个确诊病例轨迹地图，包含热点图和轨迹图，这样更直观，方便大家排查病例出现的地点，目前已上线，并受到好评，绿色免费随便用：http://www.hnzmg.com.cn/map。

#### 软件架构
软件架构说明
html5，百度地图+mui，ajax，感谢！

#### 安装教程

1.  直接下载，解压
2.  放入到web容器，比如TOMCAT
3.  要java环境，1.6以上就行，解决跨域问题，
4.  修改每个HTML文件中：var HTTP_URL="URLServlet?url=http:// **localhost:8080/map/**    为服务器URL地址。
5.  修改每个HTML文件中的百度地图KEY，需要去百度地图申请：
<script type="text/javascript" src="//api.map.baidu.com/api?v=2.0&ak=你的key"></script>

#### 使用说明

1.  直接访问http://URL目录/index.html就行
    每个病例信息存放在bl文件夹中，文件夹中是示例文件。
    list.jsp是病例索引文件，
    loc.jsp是首页热点信息
2. 或是访问我已经部署好的生产环境：
![输入图片说明](https://images.gitee.com/uploads/images/2020/0211/195935_409de584_362344.jpeg "0.jpg")

软件界面：
![输入图片说明](https://images.gitee.com/uploads/images/2020/0211/195954_f00582f6_362344.jpeg "1.jpg")
![输入图片说明](https://images.gitee.com/uploads/images/2020/0211/200008_7d71c80b_362344.jpeg "2.jpg")
![输入图片说明](https://images.gitee.com/uploads/images/2020/0211/200018_b53cc2d5_362344.jpeg "3.jpg")
![输入图片说明](https://images.gitee.com/uploads/images/2020/0211/200025_bfa54d8f_362344.jpeg "4.jpg")
![输入图片说明](https://images.gitee.com/uploads/images/2020/0211/200031_806572c3_362344.jpeg "5.jpg")
![输入图片说明](https://images.gitee.com/uploads/images/2020/0211/200039_66eee3c6_362344.jpeg "6.jpg")